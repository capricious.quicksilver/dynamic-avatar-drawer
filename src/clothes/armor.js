import {Clothes, ClothingPart, Clothing} from "./clothing";
import {Part} from "../parts/part";
import {Layer} from "../util/canvas";
import {coverNipplesIfHaveNoBreasts} from "../draw/draw";
import {
    drawPoints,
    extractPoint,
    splitCurve,
    adjust,
    breakPoint,
    clone,
} from "drawpoint/dist-esm";
//import {calcBelt} from "./accessory";
import {calcGlove} from "./gloves";
import {
	getLimbPointsNegative,
	getLimbPointsBellowPoint,
	findBetween,
} from "../util/auxiliary";

import {calcTee} from "./tops";

			 
export class CuirassPart extends ClothingPart {
    constructor(...data) {
        super({
            layer              : Layer.FRONT,
            loc                : "torso",
            reflect            : true,
           /* aboveParts         : ["parts neck", "parts torso", "decorativeParts torso"],*/
			aboveParts			: ["parts neck", "parts torso", "decorativeParts torso", "parts leg"],
			/*aboveSameLayerParts: ["groin", "parts leg"],*/
			
        }, {

		}, ...data);
    }

    renderClothingPoints(ex, ctx) {
		Clothes.simpleStrokeFill(ctx, ex, this);
		
		let {
			cleavageBot,cleavageTop,neck,shoulder,armpit,waist,hip,out,bottom,
		} = calcTee.call(this, ex);
		
		cleavageTop = adjust(cleavageTop,0,0.5);
		shoulder = adjust(shoulder,-1,0.5);
		
		armpit.cp1 = {
			x: (shoulder.x-5),
			y: (0.5*(shoulder.y-armpit.y))+armpit.y,
		}
		armpit.cp2 = void 0;
		
		bottom = {
			y:out.y-6,
			x:-0.2,
		};
		bottom.cp1 = {
			x: bottom.x * 0.5 + out.x * 0.5,
			y: bottom.y
		};
	
	
		ctx.beginPath();
		drawPoints(ctx, 
			cleavageBot,  
			cleavageTop,  
			neck, 
			shoulder, 
			//ex.chest.nipples,
			armpit,  
			waist,
			hip,
			out,
			bottom
			
		);
		ctx.fill();
		ctx.stroke();

    }
}


export class CuirassBreastPart extends ClothingPart {
    constructor(...data) {
        super({
            layer              : Layer.GENITALS,
            loc                : "chest",
            reflect            : true,
             aboveParts: ["parts chest", "decorativeParts chest"],
			 /* aboveParts         : ["parts neck", "parts torso", "decorativeParts torso"],*/
	//	aboveParts			: ["parts neck", "parts torso", "decorativeParts torso", "parts leg"],
			/*aboveSameLayerParts: ["groin", "parts leg"],*/
			
        }, {

		}, ...data);
    }

    renderClothingPoints(ex, ctx) {
		Clothes.simpleStrokeFill(ctx, ex, this);
		
		//nipples with no breasts
        if (coverNipplesIfHaveNoBreasts(ex, ctx, this)) {
            return;
        }
		
		const breast = ex.breast;
		
		ctx.beginPath();
		drawPoints(ctx, 
			breast.top,breast.tip,breast.bot,breast.in,breast.cleavage,breast.top
		);
		ctx.fill();
				
		ctx.beginPath();
		drawPoints(ctx, 
			breast.top,breast.tip,breast.bot
		);
		ctx.stroke();
		
		ctx.strokeStyle = ctx.fillStyle;
		ctx.beginPath();
		drawPoints(ctx, 
			breast.bot,breast.in,breast.cleavage,breast.top
		);
		ctx.stroke();
		
    }
}
 

export class GreavePart extends ClothingPart {
    constructor(...data) {
        super({
            layer:		Layer.FRONT,
            loc:		"feet",
            aboveParts: ["parts feet", "parts leg"], //?? copy of socks
			reflect:	false
        }, {
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
		Clothes.simpleStrokeFill(ctx, ex, this);
		const addPointsMid = [];
		if(typeof ex.quads !== "undefined"){
			addPointsMid[0] =  clone(ex.quads.top);
			addPointsMid[0].x =  ex.thigh.out.x;
			addPointsMid[1] = clone(ex.quads.out);
		}
		
		const outerPoints =  getLimbPointsNegative(ex.hip,ex.ankle.out,this.legCoverage,ex.hip,ex.thigh.out, ...addPointsMid, ex.knee.out,ex.calf.out,ex.ankle.out);
		const innerPoints = getLimbPointsBellowPoint(outerPoints[0],true,ex.groin,ex.thigh.in,ex.knee.intop,ex.knee.in,ex.calf.in,ex.ankle.in);

		if(outerPoints[1]){
			outerPoints[1]= extractPoint( outerPoints[1]);
		}
		
		//top curve
		outerPoints[0] = extractPoint( outerPoints[0]);
		outerPoints[0].cp1 = {
			x:findBetween(innerPoints[innerPoints.length-1].x,outerPoints[0].x,0.5),
			y:outerPoints[0].y+16
		};
	 		
		//bot curve
		innerPoints[0] = extractPoint( innerPoints[0]);
		innerPoints[0].cp1 = {
			x:findBetween(outerPoints[outerPoints.length-1].x,innerPoints[0].x,0.5),
			y:innerPoints[0].y+6
		};
				
		ctx.beginPath();
		drawPoints(ctx,  ...outerPoints, ...innerPoints, outerPoints[0]);
		ctx.fill();
		ctx.stroke();
    }
}


export class VambracePart extends ClothingPart {
    constructor(...data) {
        super({
            layer     : Layer.ARMS,
            loc       : "arm",
            aboveParts: ["parts arm", "decorativeParts arm", "parts hand"],
        }, {
            armCoverage: 0.5,
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
		Clothes.simpleStrokeFill(ctx, ex, this);
		
		const {outerArmPoints,innerArmPoints} = calcGlove.call(this,ex);
		ctx.beginPath();
		drawPoints(ctx, 
			...outerArmPoints,
			extractPoint(ex.wrist.out),
			ex.wrist.in,
			...innerArmPoints,
			extractPoint(outerArmPoints[0])
		);
		ctx.fill();
		ctx.stroke();
    }
}


/**
 * Base Clothing classes
 */
export class Armor extends Clothing {
    constructor(...data) {
        super({
            clothingLayer  : Clothes.Layer.OUTER,//TODO
        }, ...data);
    }
	
	 stroke() {
        return "#252539";
    }

    fill() {
        return "#8b8e91";
    }
}



export class Cuirass extends Armor {
    constructor(...data) {
        super({
			cleavageOpeness: 0.055,
			cleavageCoverage: 0.1,
			sideLoose: 0.3,
			waistCoverage: 0.5,
			curveCleavageX:1,
			curveCleavageY:-4,
			legCoverage: 1, //used only by DressBreastPart but important
        }, ...data);
    }

    get partPrototypes() {
        return [
			{
                side: null,
                Part: CuirassBreastPart
            },{ 
                side: null,  
                Part: CuirassPart
            },
			
        ];
    }
}

export class Greaves extends Armor {
    constructor(...data) {
        super({
			legCoverage: 0.54,
        }, ...data);
    }

    get partPrototypes() {
        return [
          {
                side: Part.LEFT,
                Part: GreavePart
            },
			{
                side: Part.RIGHT,
                Part: GreavePart
            },
        ];
    }
}

export class Vambraces extends Armor {
    constructor(...data){
        super({
			armCoverage:0.60,
		},...data);
    }

	
    get partPrototypes(){
        return [
           	{
                side: Part.LEFT,
                Part: VambracePart,
            },
            {
                side: Part.RIGHT,
                Part: VambracePart,
            },	
        ];
    }
}
