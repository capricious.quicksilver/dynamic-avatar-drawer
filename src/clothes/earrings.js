import {Clothes, ClothingPart, Clothing, processClothingPartLocation} from "./clothing";
import {Layer} from "../util/canvas";
import {locateRelativeToDrawpoint, Location} from "..";
import {
    drawPoints,
    rad,
    point,
   //norm,
   //diff,
	adjust,
	drawCircle,
	//extractPoint,
    //splitCurve,
    //reflect,
	
} from "drawpoint/dist-esm";

import {Jewelry} from "./jewelry";

import {
	findBetween,
	drawStar,
	perpendicularPoint,
	polar2cartesian,
	cartesian2polar,
} from "../util/auxiliary";


export class TriangleEarringsPart extends ClothingPart {
    constructor(...data) {
        super({
            layer              : Layer.BELOW_HAIR,
            loc                : "+head",
            reflect            : true,
        }, {			
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
			Clothes.simpleStrokeFill(ctx, ex, this);		
			
			const center = adjust(ex.skull.bot,1.2,2.2);
			const top = adjust(center,0,-this.hanger);
			const botIn = adjust(top,this.width,-this.length);
			const botOut = adjust(top,-this.width,-this.length);
			
			//connecting line
			ctx.beginPath();
			ctx.lineWidth = 0.5;
			drawPoints(ctx,center,{x:top.x, y:top.y-2});
			ctx.stroke();
			
			//circle attached to the ear
			let points = drawCircle(center,0.3);
			ctx.beginPath();
			drawPoints(ctx,...points);
			ctx.stroke();
			
			//the triangle part
			ctx.beginPath();
			drawPoints(ctx,top,botIn,botOut);
			ctx.fill();
				
    }
}


export class CrystalEarringsPart extends ClothingPart {
    constructor(...data) {
        super({
            layer              : Layer.BELOW_HAIR,
            loc                : "+head",
            reflect            : true,
        }, {			
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
			Clothes.simpleStrokeFill(ctx, ex, this);		
			
			const center = adjust(ex.skull.bot,1.2,2.2);
			var topCenter = adjust(center,0,-this.hanger);
			
			var topOut = adjust(topCenter,-this.width/2+0.3,0);
			var topIn = adjust(topCenter,this.width/2,0);
			
			var botOut = adjust(topCenter,-this.width/2+0.3,-this.length-this.alt);
			var botIn = adjust(topCenter,this.width/2,-this.length+this.alt);
			
			
			//connecting line
			ctx.beginPath();
			ctx.lineWidth = 0.5;
			drawPoints(ctx,center,topCenter);
			ctx.stroke();
			
			//circle attached to the ear
			let points = drawCircle(center,0.3);
			ctx.beginPath();
			drawPoints(ctx,...points);
			ctx.stroke();
			
			//the crystal part
			ctx.beginPath();
			drawPoints(ctx,topOut,topIn,botIn,botOut);
			ctx.fill();
				
    }
}


export class LoopEarringsFrontPart extends ClothingPart {
    constructor(...data) {
        super({
            layer              : Layer.FRONT,
            loc                : "+head",
            reflect            : true,
        }, {			
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
			Clothes.simpleStrokeFill(ctx, ex, this);		
			const {a,b} = calcLoop.call(this, ex);
			
			ctx.beginPath();
			drawPoints(ctx,b,a);
			ctx.stroke();		
    }
}

export class LoopEarringsBackPart extends ClothingPart {
    constructor(...data) {
        super({
            layer              : Layer.BELOW_HAIR,
            loc                : "+head",
            reflect            : true,
        }, {			
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
			Clothes.simpleStrokeFill(ctx, ex, this);		
			const {a,b} = calcLoop.call(this, ex);
			
			ctx.beginPath();
			drawPoints(ctx,a,b);
			ctx.stroke();		
    }
}

function calcLoop(ex){
	const center = adjust(ex.skull.bot,1.2,3);
	let a = {x:center.x,y:center.y};
	let b = {x:center.x,y:center.y-this.length};
	a.cp1 = {x:center.x-this.width,y:center.y-(this.length/2)};
	b.cp1 = {x:center.x+this.width,y:center.y-(this.length/2)};
	
	return{
		a,b
	};
}


export class ChainEarringsPart extends ClothingPart {
    constructor(...data) {
        super({
            layer              : Layer.BELOW_HAIR,
            loc                : "+head",
            reflect            : true,
        }, {			
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
			Clothes.simpleStrokeFill(ctx, ex, this);		
			
			const center = adjust(ex.skull.bot,1.2,2.2);
			
			function chain(x,y,length){
				var top = adjust(center,x,y)
				var bot = adjust(center,x,-length)
				ctx.setLineDash([2, 2])
				ctx.lineWidth = 0.4;
				ctx.beginPath();
				drawPoints(ctx,top,bot);
				ctx.stroke();
			}
					
			chain(0,0,this.length);
			chain(0.8,0,this.length);
			chain(-0.8,0,this.length);
			
			let points = [];
			points = drawCircle(center,1.2); 
			ctx.beginPath();
			drawPoints(ctx,...points);
			ctx.fillStyle = ctx.strokeStyle;
			ctx.fill();		
    }
}


export class BallEarringsPart extends ClothingPart {
    constructor(...data) {
        super({
            layer              : Layer.BELOW_HAIR,
            loc                : "+head",
            reflect            : true,
        }, {			
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
			Clothes.simpleStrokeFill(ctx, ex, this);		
			
			const center = adjust(ex.skull.bot,1.2,2.2);
			const mid = adjust(center,0,-this.length/2);
			const bottom = adjust(center,0,-this.length);
									
			//connecting line
			ctx.beginPath();
			ctx.lineWidth = this.thickness;
			drawPoints(ctx,center,bottom);
			ctx.stroke();
						
			//balls
			ball(center,this.size,ctx.fillStyle)
			ball(mid,this.size-this.alt,ctx.fillStyle)
			ball(bottom,this.size+this.alt,ctx.fillStyle)
			
			function ball(center,radius,fill){
				let points = [];
				points = drawCircle(center,radius); 
				ctx.beginPath();
				drawPoints(ctx,...points);
				ctx.fillStyle = fill;
				ctx.fill();		
			} 			
    }
}

export class RhombEarringsFrontPart extends ClothingPart {
    constructor(...data) {
        super({
            layer              : Layer.BELOW_HAIR,
            loc                : "+head",
            reflect            : true,
        }, {			
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
			Clothes.simpleStrokeFill(ctx, ex, this);		
			const {center,inner,bottom,outer} = calcRhomb.call(this, ex);
			
			ctx.beginPath();
			ctx.lineWidth = this.thickness;
			drawPoints(ctx,center,outer,bottom);
			ctx.stroke();
    }
}

export class RhombEarringsBackPart extends ClothingPart {
    constructor(...data) {
        super({
            layer              : Layer.FRONT,
            loc                : "+head",
            reflect            : true,
        }, {			
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
			Clothes.simpleStrokeFill(ctx, ex, this);		
			const {center,inner,bottom,outer} = calcRhomb.call(this, ex);
			
			ctx.beginPath();
			ctx.lineWidth = this.thickness;
			drawPoints(ctx,center,inner,bottom);
			ctx.stroke();
    }
}

function calcRhomb(ex){
	const center = adjust(ex.skull.bot,1.2,2.2);
	const bottom = adjust(center,0,-this.length);
	
	let actualLength = -this.length/2 - this.length/2*this.alt;
	const inner = adjust(center, -(this.width/2), actualLength);
	const outer = adjust(center, this.width/2, actualLength);

	return{
		center,
		inner,
		bottom,
		outer
	};
}

export class EarPiercingFrontPart extends ClothingPart {
    constructor(...data) {
        super({
            layer              : Layer.FRONT,
            loc                : "+head",
            reflect            : false,
        }, {			
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
			Clothes.simpleStrokeFill(ctx, ex, this);		
			const {a,b} = calcEarPiercing.call(this, ex);
			
			ctx.beginPath();
			drawPoints(ctx,b,a);
			ctx.stroke();		
    }
}

export class EarPiercingBackPart extends ClothingPart {
    constructor(...data) {
        super({
            layer              : Layer.BELOW_HAIR,
            loc                : "+head",
            reflect            : false,
        }, {			
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
			Clothes.simpleStrokeFill(ctx, ex, this);		
			const {a,b} = calcEarPiercing.call(this, ex);
			
			ctx.beginPath();
			drawPoints(ctx,a,b);
			ctx.stroke();		
			
    }
}

function calcEarPiercing(ex){
	const center = adjust(ex.ear.mid,-0.5+this.dx,3.5+this.dy)
	
	let a = {x:center.x,y:center.y};
	let b =  polar2cartesian(this.length,rad(this.rotation),a);

	a.cp1 = perpendicularPoint(a,b,0.5,this.width);
	b.cp1 = perpendicularPoint(a,b,0.5,-this.width);

	return{
		a,b
	};
}

/**
 * Base Clothing classes
 */
export class Earrings extends Jewelry {
    constructor(...data) {
        super({
            clothingLayer: Clothes.Layer.INNER,
            stroke       : "#ebbc11",
            fill         : "red",
        }, ...data);
		
    }
}
/**/

export class TriangleEarrings extends Earrings {
    constructor(...data) {
        super({
			length: 7,
			width: 2,
			hanger: 2
        }, ...data);
    }

    get partPrototypes() {
        return [
            {
                side: null,
                Part: TriangleEarringsPart,
            }
        ];
    }
}

export class CrystalEarrings extends Earrings {
    constructor(...data) {
        super({
			length: 9,
			width: 2.6,
			hanger: 3,
			alt: 2,
        }, ...data);
    }

    get partPrototypes() {
        return [
            {
                side: null,
                Part: CrystalEarringsPart,
            }
        ];
    }
}

export class LoopEarrings extends Earrings {
    constructor(...data) {
        super({
			length: 9,
			width: 3,
        }, ...data);
    }

    get partPrototypes() {
        return [
           {
                side: null,
                Part: LoopEarringsFrontPart,
            },{
                side: null,
                Part: LoopEarringsBackPart,
            }
        ];
    }
}

export class ChainEarrings extends Earrings {
    constructor(...data) {
        super({
			length: 9,
        }, ...data);
    }

    get partPrototypes() {
        return [
            {
                side: null,
                Part: ChainEarringsPart,
            }
        ];
    }
}

export class BallEarrings extends Earrings {
    constructor(...data) {
        super({
			length: 10,
			size: 1,
			alt: -0.2,
			thickness: 0.5
        }, ...data);
    }

    get partPrototypes() {
        return [
            {
                side: null,
                Part: BallEarringsPart,
            }
        ];
    }
}

export class RhombEarrings extends Earrings {
    constructor(...data) {
        super({
			length: 9,
			width: 2.6,
			alt: 0.8,
			thickness: 0.8,
        }, ...data);
    }

	stroke() {
        return "#13a700";
    }
	
    get partPrototypes() {
        return [
            {
                side: null,
                Part: RhombEarringsFrontPart,
            },{
                side: null,
                Part: RhombEarringsBackPart,
            }
        ];
    }
}

export class EarPiercing extends Earrings {
    constructor(...data) {
        super({
			length: 2.8,
			width: 2.2,
			thickness: 0.6,
			rotation: 46,
			dx: 0,
			dy: 0,
        }, ...data);
    }

	get partPrototypes() {
        return [
           {
                side: null,
                Part: EarPiercingFrontPart,
            },{
                side: null,
                Part: EarPiercingBackPart,
            }
        ];
    }
}