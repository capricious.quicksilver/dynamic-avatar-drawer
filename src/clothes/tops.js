import {Clothes, ClothingPart, Clothing} from "./clothing";
import {Part} from "../parts/part";
import {coverNipplesIfHaveNoBreasts} from "../draw/draw";
import {Layer} from "../util/canvas";
//import {setStrokeAndFill} from "../util/draw";
import {
	extractPoint,
	drawPoints, 
	splitCurve,
	breakPoint,
	clone,
	adjust,
	simpleQuadratic,
} from "drawpoint/dist-esm";
import {
	straightenCurve,
	findBetween,
	lineLineIntersection,
	getLimbPointsNegative,
	//getLacingPoints,
} from "../util/auxiliary";

import {DressBreastPart,SuperSleevePart,DetachedSleevePart,calcDressCleavage,LacingPart} from "./dress";


//basically calcDressBase but only goes to waist
export function calcTee(ex){
	//arm
	let shoulder = clone(ex.collarbone);
	
	//waist points
	const {armpit,lat,waist,hip,out,bottom} = calcTopBody.call(this, ex);
	
	//cleavage
	const {
		cleavageBot,cleavageTop,neck
	} = calcDressCleavage.call(this,ex);
	//TODO - checking of bottom limit was removed because DressBreastPart calculate it differently (based on legCoverage)
	//it is assumed the tee won't be used with such deep cleavage for this to matter 
	return {
			cleavageBot: cleavageBot,
			cleavageTop: cleavageTop,
			neck:neck,
			shoulder:shoulder,
			
			armpit:armpit,
			lat:lat,
			waist: waist,
			hip: hip, 
			out:out, 
			bottom: bottom
		};
}


export class TeePart extends ClothingPart {
    constructor(...data) {
        super({
            layer              : Layer.MIDRIFT,
            loc                : "torso",
            reflect            : true,
           /* aboveParts         : ["parts neck", "parts torso", "decorativeParts torso"],*/
            aboveParts			: ["parts neck", "parts torso", "decorativeParts torso", "parts leg"],
			/*aboveSameLayerParts: ["groin", "parts leg"],*/
			
        }, {

		}, ...data);
    }

    renderClothingPoints(ex, ctx) {

		const {
			cleavageBot,cleavageTop,neck,shoulder,armpit,waist,hip,out,bottom,
		} = calcTee.call(this, ex);
		
		 Clothes.simpleStrokeFill(ctx, ex, this);

		ctx.beginPath();
		drawPoints(ctx, 
			cleavageBot,  
			cleavageTop,  
			neck, 
			shoulder, 
			armpit,  
			waist,
			hip,
			out,
			bottom
			
		);
		ctx.fill();
		ctx.stroke();

    }
}

//basically Tee+SuperPanties
export class LeotardPart extends ClothingPart {
    constructor(...data) {
        super({
            layer              : Layer.FRONT,
            loc                : "torso",
            reflect            : true,
           /* aboveParts         : ["parts neck", "parts torso", "decorativeParts torso"],*/
            aboveParts			: ["parts neck", "parts torso", "decorativeParts torso", "parts leg"],
			/*aboveSameLayerParts: ["groin", "parts leg"],*/
			
        }, {

		}, ...data);
    }

    renderClothingPoints(ex, ctx) {

		const {
			cleavageBot,cleavageTop,neck,shoulder,armpit,waist,hip,out,bottom,
		} = calcTee.call(this, ex);
		
		
		//bottom
		let bot = adjust(clone(ex.groin),-0.2,0);		
		
		let temp = splitCurve(this.genCoverage,ex.groin,extractPoint(ex.thigh.top));	
		let botOut = extractPoint(temp.left.p2);
		
		//bottom curve (hip ? hip : waist)
		temp = splitCurve(0.5, out, botOut );	
		botOut.cp1 = extractPoint(temp.left.p2);
		botOut.cp1.x+=this.curveBotX-9;
		botOut.cp1.y+=this.curveBotY+5;
	
		Clothes.simpleStrokeFill(ctx, ex, this);

		ctx.beginPath();
		drawPoints(ctx, 
			cleavageBot,  
			cleavageTop,  
			neck, 
			shoulder, 
			armpit,  
			waist,
			hip,
			out,
			botOut,
			bot,
			//out,
			//bottom
			
		);
		ctx.fill();
		ctx.stroke();

    }
}


export class HalterTopBreastPart extends ClothingPart {
    constructor(...data) {
        super({
            layer              : Layer.GENITALS,
            loc                : "chest",
            reflect            : true,
           aboveParts: ["parts chest", "decorativeParts chest"]
        }, {
            
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
		Clothes.simpleStrokeFill(ctx, ex, this);

		//check and recalculate cleavage if deep beyond limit
		function checkCleavage(context,bottom){
			if(cleavage.y<bottom.y){
				cleavage.y = bottom.y+3;
			}
						
			sp = splitCurve(0.5,cleavage,topIn);
			topIn.cp1 = {
				x: sp.left.p2.x+context.curveCleavageX,
				y: sp.left.p2.y+context.curveCleavageY,
			};
		}
		
		//CLEAVAGE
		let sp = splitCurve(this.cleavageCoverage,ex.neck.cusp, ex.groin);
		let cleavage = {
			x : -0.2,
			y : sp.left.p2.y
		};
		
		if(this.outerNeckCoverage<this.innerNeckCoverage){
			this.outerNeckCoverage = this.innerNeckCoverage;
		}
		if(this.innerNeckCoverage>this.outerNeckCoverage){
			this.innerNeckCoverage = this.outerNeckCoverage;
		}
		
		const cusp = ex.trapezius ? ex.trapezius : ex.neck.cusp;
		sp = splitCurve(this.outerNeckCoverage,cusp, ex.collarbone);
		const topOut = this.outerNeckCoverage > 1 ? ex.collarbone : sp.left.p2;
				
		if(this.innerNeckCoverage<0){
			sp = splitCurve(1+this.innerNeckCoverage, ex.neck.top, cusp);
		}else{
			sp = splitCurve(this.innerNeckCoverage, cusp, ex.collarbone);
		}
		const topIn = extractPoint(sp.left.p2);
				
		const topMid = this.innerNeckCoverage < 0 ? cusp : void 0;
		
		const armpit = extractPoint(ex.armpit);
		
		//NO BREASTS
		if (ex.hasOwnProperty("breast") === false) {
			
			let bottom = {
				x: -0.2,
				y: armpit.y-1
			};		
			//if(cleavage.y<bottom.y)cleavage.y=bottom.y+3;
			checkCleavage(this,bottom);
			
			ctx.beginPath();
			drawPoints(ctx,
				cleavage,
				topIn,
				topMid,
				topOut,
				armpit,
				bottom
			);
			ctx.fill();
			
			ctx.beginPath();
			drawPoints(ctx,
				cleavage,
				topIn,
				topMid,
				topOut,
				armpit,
			);	
			ctx.stroke();
			
			coverNipplesIfHaveNoBreasts(ex, ctx, this)
			
			return;
		}
		
		//BREASTS		
		//calculate points - I really dunno how, but it do 
		const tip = adjust(ex.breast.tip, 0.1, 0);
		const bot = adjust(ex.breast.bot, 0, -0.1);
		
		let bottom = {
			x: -0.2,
			y: bot.y
		};
		
		//if(cleavage.y<bottom.y)cleavage.y=bottom.y+3;
		checkCleavage(this,bottom);
		
		ctx.beginPath();
		drawPoints(ctx,
			cleavage,
			topIn,
			topMid,
			topOut,
			//top,
			tip,
			bot,
			bottom
		);
		ctx.fill();
		
		ctx.beginPath();
		drawPoints(ctx,
			cleavage,
			topIn,
			topMid,
			topOut,
			//top,
			tip,
			bot
		);
		ctx.stroke();
		
    }
}


export class TubeTopBreastPart extends ClothingPart {
    constructor(...data) {
        super({
            layer              : Layer.GENITALS,
            loc                : "chest",
            reflect            : true,
           aboveParts: ["parts chest", "decorativeParts chest"]
        }, {
            
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
		if (coverNipplesIfHaveNoBreasts(ex, ctx, this)) {
            return;
        }
		Clothes.simpleStrokeFill(ctx, ex, this);

		let topOut = [];
		//for transformations:
		if(this.chestCoverage>1){
			if(this.chestCoverage>2)this.chestCoverage=1.9;
			let coverage = 1-(this.chestCoverage-1)
			topOut = getLimbPointsNegative(ex.neck.top,ex.breast.top,1-(this.chestCoverage-1),ex.neck.top,ex.neck.cusp,ex.collarbone, extractPoint(ex.breast.top) );		
			topOut[0] = extractPoint(topOut[0]);
			topOut[topOut.length] = adjust(ex.breast.tip, 0.1, 0); 
		//normal:
		}else{
			let temp = splitCurve(1-this.chestCoverage,ex.breast.top,ex.breast.tip);
			topOut[0] = extractPoint(temp.left.p2);
			topOut[1] = adjust(temp.right.p2, 0.1, 0); //tip
			
		}
		
		const topIn = {
			x : -0.2,
			y : topOut[0].y + 1,
		};
		
		topOut[0].cp1 = {
			x: (topOut[0].x+topIn.x) * 0.8,
			y: topIn.y
		};
	
		const bot = adjust(ex.breast.bot, 0, -0.1);
		
		const bottom = {
			x: -0.2,
			y: bot.y
		};
		
		ctx.beginPath();
		drawPoints(ctx,
			topIn,
			...topOut,
			bot,
			bottom
		);
		ctx.fill();
		
		ctx.beginPath();
		drawPoints(ctx,
			topIn,
			...topOut,
			bot
		);
		ctx.stroke();
		
    }
}


export class TopChestPart extends ClothingPart {
    constructor(...data) {
        super({
            layer              : Layer.MIDRIFT,
            loc                : "torso",
            reflect            : true,
           /* aboveParts         : ["parts neck", "parts torso", "decorativeParts torso"],*/
            aboveParts			: ["parts neck", "parts torso", "decorativeParts torso", "parts leg"],
			/*aboveSameLayerParts: ["groin", "parts leg"]*/
        }, {
            
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
		Clothes.simpleStrokeFill(ctx, ex, this);
		
		if(this.waistCoverage>=2)return;
		
		//waist points
		const {armpit,lat,waist,hip,out,bottom} = calcTopBody.call(this, ex);
		
		const top = {x: -0.2}
		top.y = ex.breast ? ex.breast.in.y : ex.armpit.y;
				
		ctx.beginPath();
		drawPoints(ctx, 
			armpit,  
			lat,
			waist,
			hip,
			out,
			bottom,
			top,
			extractPoint(armpit)
		);
		ctx.fill();
		
		ctx.beginPath();
		drawPoints(ctx, 
			armpit, 
			lat,
			waist,
			hip,
			out,
			bottom
		);
		ctx.stroke();
		
    }
}

export class TopGroinPart extends ClothingPart {
    constructor(...data) {
        super({
            layer              : Layer.FRONT,
            loc                : "torso",
            reflect            : true,
           /* aboveParts         : ["parts neck", "parts torso", "decorativeParts torso"],*/
            aboveParts			: ["parts neck", "parts torso", "decorativeParts torso", "parts leg"],
			/*aboveSameLayerParts: ["groin", "parts leg"]*/
        }, {
            
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
		Clothes.simpleStrokeFill(ctx, ex, this);
		
		if(this.waistCoverage>=2)return;
		
		//waist points
		const {armpit,lat,waist,hip,out,bottom} = calcTopBody.call(this, ex);
		
		const top = {x: -0.2}
		top.y = ex.breast ? ex.breast.in.y : ex.armpit.y;
		
		//bottom
		let bot = adjust(clone(ex.groin),-0.2,0);		
		
		let temp = splitCurve(this.genCoverage,ex.groin,extractPoint(ex.thigh.top));	
		let botOut = extractPoint(temp.left.p2);
		
		//bottom curve (hip ? hip : waist)
		temp = splitCurve(0.5, out, botOut );	
		botOut.cp1 = extractPoint(temp.left.p2);
		botOut.cp1.x+=this.curveBotX-9;
		botOut.cp1.y+=this.curveBotY+5;
		
		ctx.beginPath();
		drawPoints(ctx, 
			armpit,  
			lat,
			waist,
			hip,
			out,
			botOut,
			bot,
			top,
			extractPoint(armpit)
		);
		ctx.fill();
		
		ctx.beginPath();
		drawPoints(ctx, 
			armpit, 
			lat,
			waist,
			hip,
			out,
			botOut,
			bot,
		);
		ctx.stroke();
		
    }
}

export function calcTopBody(ex){
	let armpit = clone(ex.armpit);
	let lat = clone(ex.lat);
	//looseness of waist points 
	let hip = adjust(ex.hip, 0, 0);
	let waist = adjust(ex.waist,(this.thickness * 0.8), 0); //last remnant of sweater
	
	
	//to have top loose around the waist (the same function as for dress)
	{
		let top = armpit;
		if(lat)top = lat;
		let mid = lineLineIntersection(top,hip,{x:0,y:waist.y},{x:100,y:waist.y});
		if(mid.x>waist.x){
			waist.x = findBetween(waist.x, mid.x,this.sideLoose);
			straightenCurve(armpit,waist,this.sideLoose);
			straightenCurve(waist,hip,this.sideLoose);
		};
	}
	
	let out;
	//coverage of waist
	if (this.waistCoverage > 1){
		let sp = splitCurve(1-(this.waistCoverage-1),ex.armpit,waist); //THIS????????
		waist = void 0;
		hip = void 0;
		out = sp.left.p2;
	}else if (this.waistCoverage >= 0){
		let sp = splitCurve(1-this.waistCoverage,waist,hip);
		//waist =  adjust(ex.hip, 0, 0);
		hip = void 0;
		out = sp.left.p2;
    }else{
        let sp = splitCurve(Math.abs(this.waistCoverage),hip,ex.thigh.out);
		out = sp.left.p2;
    }
	
	//bottom
	let bottom = {
			y:out.y-3,
			x:-0.2,
		};
	
	bottom.cp1 = {
		x: bottom.x * 0.5 + out.x * 0.5,
		y: bottom.y
	};
	
	return {
		armpit:armpit,
		lat:lat,
		waist: waist,
		hip: hip, 
		out:out, 
		bottom: bottom
	};
}

export class BikiniTopBreastPart extends ClothingPart {
    constructor(...data) {
        super({
            layer              : Layer.GENITALS,
            loc                : "chest",
            reflect            : true,
           aboveParts: ["parts chest", "decorativeParts chest"]
        }, {
            
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
		Clothes.simpleStrokeFill(ctx, ex, this);
		
		if(this.outerNeckCoverage<this.innerNeckCoverage)this.outerNeckCoverage=this.innerNeckCoverage;
		if(this.innerNeckCoverage>this.outerNeckCoverage)this.innerNeckCoverage=this.outerNeckCoverage;
		
		let cusp = ex.neck.cusp;
		if(ex.trapezius)cusp=ex.trapezius;
		let sp = splitCurve(this.outerNeckCoverage,cusp, ex.collarbone);
		let topOut = sp.left.p2;
				
		if(this.innerNeckCoverage<0){
			sp = splitCurve(1+this.innerNeckCoverage, ex.neck.top, cusp);
		}else{
			sp = splitCurve(this.innerNeckCoverage, cusp, ex.collarbone);
		}
		let topIn = extractPoint(sp.left.p2);
				
		
		//NO BREASTS
		if (ex.hasOwnProperty("breast") === false) {
			let armpit = adjust(ex.armpit,0,0);
			let bottom = {
				x: -0.2,
				y: armpit.y-1
			};

			let cleavage = {
				x : -0.2,
				y : bottom.y + (this.radius*2)
			};
			cleavage.cp1 = {
				x: bottom.x + (this.radius*2),
				y: bottom.y + this.radius
			}
			
			
			sp = splitCurve(0.5,cleavage,topIn);
			topIn.cp1 = {
				x: sp.left.p2.x+this.curveCleavageX,
				y: sp.left.p2.y+this.curveCleavageY,
			};
			
			ctx.beginPath();
			drawPoints(ctx,
				cleavage,
				topIn,
				topOut,
				extractPoint(armpit),
				bottom
			);
			ctx.fill();
			
			ctx.beginPath();
			drawPoints(ctx,
				cleavage,
				topIn,
				topOut,
				extractPoint(armpit)
			);	
			ctx.stroke();
			
			coverNipplesIfHaveNoBreasts(ex, ctx, this)
			
			return;
		}
		
		//BREASTS		
		//calculate points - I really dunno how, but it do 
		const tip = adjust(ex.breast.tip, 0.1, 0);
		const bot = adjust(ex.breast.bot, 0, -0.1);
		
		let bottom = {
			x: -0.2,
			y: bot.y
		};
		
		let cleavage = {
			x : -0.2,
			y : bottom.y + (this.radius*2)
		};
		cleavage.cp1 = {
			x: bottom.x + (this.radius*2),
			y: bottom.y + this.radius
		}
			
		sp = splitCurve(0.5,cleavage,topIn);
		topIn.cp1 = {
				x: sp.left.p2.x+this.curveCleavageX,
				y: sp.left.p2.y+this.curveCleavageY,
		};
			
		ctx.beginPath();
		drawPoints(ctx,
			cleavage,
			topIn,
			topOut,
			//top,
			tip,
			bot,
			bottom,
			cleavage,
		);
		ctx.fill();
		
		ctx.beginPath();
		drawPoints(ctx,
			cleavage,
			topIn,
			topOut,
			//top,
			tip,
			bot,
			bottom,
			cleavage,
		);
		ctx.stroke();
		
    }
}

/**
 * Base Clothing classes
 */
 
export class Top extends Clothing {
    constructor(...data) {
		super({
			clothingLayer  : Clothes.Layer.MID,
			thickness: 0.6,
		}, ...data);
    }
	
	stroke() {
        return "hsla(335, 800%, 30%, 1)";
    }

    fill() {
        return "hsla(335, 100%, 42%, 1)";
    }
}
	
 
export class Tee extends Top {
    constructor(...data) {
        super({
			cleavageOpeness: 0.3,
			cleavageCoverage: 0.16,
			sideLoose: 0,
			waistCoverage: 0,
			curveCleavageX:0,
			curveCleavageY:0,
			legCoverage: 1, //used only by DressBreastPart but important
        }, ...data);
    }

    get partPrototypes() {
        return [
			{ 
                side: null,  
                Part: LacingPart
            },{
                side: null,
                Part: TeePart
            },{ 
                side: null,  
                Part: DressBreastPart
            },{
                side: Part.LEFT,
                Part: SuperSleevePart
            },{
                side: Part.RIGHT,
                Part: SuperSleevePart
            },
			
        ];
    }
}
	
export class Leotard extends Top {
    constructor(...data) {
        super({
			cleavageOpeness: 0.2,
			cleavageCoverage: 0.1,
			waistCoverage: 0.5,
			curveCleavageX: 2,
			curveCleavageY: -2,
			armCoverage: 0.8,
			sideLoose: 0,
			genCoverage: 1,
			curveBotX: 6,
			curveBotY: 6,
		
			legCoverage: 1, //used only by DressBreastPart but important
        }, ...data);
    }

    get partPrototypes() {
        return [
			{
                side: null,
                Part: LeotardPart
            },{ 
                side: null,  
                Part: DressBreastPart
            },{
                side: Part.LEFT,
                Part: SuperSleevePart
            },{
                side: Part.RIGHT,
                Part: SuperSleevePart
            },
			
        ];
    }
}

export class HalterTop extends Top {
    constructor(...data) {
        super({
			cleavageCoverage: 0.3,
			outerNeckCoverage: 0.35,
			innerNeckCoverage: 0.15,
			curveCleavageX: 9,
			curveCleavageY: -9,
			waistCoverage: 0.66,
			sideLoose: 0,
        }, ...data);
    }

    get partPrototypes() {
        return [
          {
                side: null,
                Part: HalterTopBreastPart
            },{ 
                side: null,  
                Part: TopChestPart
           }
        ];
    }
}	


export class TubeTop extends Top {
    constructor(...data) {
        super({
			chestCoverage: 0.3,
			waistCoverage: 0.3,
			sideLoose: 0,
        }, ...data);
    }

    get partPrototypes() {
        return [
			{
                side: null,
                Part: TubeTopBreastPart
            },{ 
                side: null,  
                Part: TopChestPart
            },

        ];
    }
}


export class TubeTopSleeves extends Top {
    constructor(...data) {
        super({
			chestCoverage: 0.3,
			waistCoverage: 0.3,
			sideLoose: 0,
			
			shoulderCoverage: 0,
			armCoverage: 0.5,
			armLoose: 0.5,
        }, ...data);
    }

    get partPrototypes() {
        return [
			{
                side: null,
                Part: TubeTopBreastPart
            },{ 
                side: null,  
                Part: TopChestPart
            },{
                side: Part.LEFT,
                Part: DetachedSleevePart
            },{
                side: Part.RIGHT,
                Part: DetachedSleevePart
            },

        ];
    }
}

export class BikiniTop extends Top {
    constructor(...data) {
        super({
			radius: 3.6,
			outerNeckCoverage: 0.35,
			innerNeckCoverage: 0.03,
			curveCleavageX: 14,
			curveCleavageY: -14,
        }, ...data);
    }

    get partPrototypes() {
        return [
          {
                side: null,
                Part: BikiniTopBreastPart
            }
        ];
    }
}

export class Swimsuit extends Top {
    constructor(...data) {
        super({
			cleavageCoverage: 0.3,
			outerNeckCoverage: 0.35,
			innerNeckCoverage: 0.15,
			curveCleavageX: 9,
			curveCleavageY: -9,
			waistCoverage: 0.2,
			sideLoose: 0,
			genCoverage: 1,
			curveBotX: 6,
			curveBotY: 6,
        }, ...data);
    }

    get partPrototypes() {
        return [
          {
                side: null,
                Part: HalterTopBreastPart
            },{ 
                side: null,  
                Part: TopGroinPart
           }
        ];
    }
}	
