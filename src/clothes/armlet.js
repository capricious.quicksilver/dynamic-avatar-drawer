import {Clothes, ClothingPart, Clothing, processClothingPartLocation} from "./clothing";
import {Layer} from "../util/canvas";
import {locateRelativeToDrawpoint, Location, Part} from "..";
import {
    drawPoints,
    splitCurve,
    rad,
    drawCircle,
    breakPoint,
    extractPoint,
    adjust,
    reflect,
    /*
    point,
    norm,
    diff,
    */
} from "drawpoint/dist-esm";

import {Jewelry} from "./jewelry";

import {
    findBetween,
    getLimbPointsBellowPoint,
    getLimbPointsAbovePoint,
    getLacingPoints
} from "../util/auxiliary";

function calcArm(ex, controlPoint) {
    if (typeof controlPoint !== "undefined" && typeof controlPoint !== "object") {
        controlPoint = {x: 0, y: controlPoint};
    }

    let temp = getLimbPointsBellowPoint(controlPoint, true, ex.armpit, ex.elbow.in, ex.wrist.in, ex.thumb.out,
        ex.thumb.tip);
    const inner = extractPoint(temp[temp.length - 1]);

    temp = getLimbPointsBellowPoint(adjust(inner, 0, 2), false, ex.collarbone, ex.deltoids, ex.shoulder, ex.elbow.out,
        ex.wrist.out, ex.hand.knuckle);
    const outer = extractPoint(temp[0]);

    outer.cp1 = {
        x: outer.x * 0.5 + inner.x * 0.5,
        y: outer.y - 3
    };

    return {
        inner,
        outer
    }
}


function calcArmSquare(ex) {
    if ((this.armCoverage - this.armCoverageTop) < 0.03) {
        this.armCoverageTop = this.armCoverage - 0.03;
    }

    let controlPoint = {
        y: findBetween(ex.collarbone.y, ex.hand.palm.y, this.armCoverageTop)
    };

    let inner = getLimbPointsBellowPoint(controlPoint, true, ex.armpit, ex.elbow.in, ex.wrist.in, ex.thumb.out,
        ex.thumb.tip);
    let outer = getLimbPointsBellowPoint(adjust(inner[inner.length - 1], 0, 2), false, ex.collarbone, ex.deltoids,
        ex.shoulder, ex.elbow.out, ex.wrist.out, ex.hand.knuckle);


    controlPoint = {
        y: findBetween(ex.collarbone.y, ex.hand.palm.y, this.armCoverage)
    };


    inner.reverse(); //to getLimpPoints functions they have to be inputed sorted from top to bottom (to keep the same style as outer points), but they are outputted from bottom to top (as they are actually drawn)
    inner = getLimbPointsAbovePoint(controlPoint, true, ...inner);

    outer = getLimbPointsAbovePoint({y: controlPoint.y + 2}, false, ...outer);

    outer[0] = extractPoint(outer[0]);

    outer[0].cp1 = {
        x: (outer[0].x + inner[inner.length - 1].x) * 0.5,
        y: outer[0].y - 4 + (8 * this.armCoverageTop)
    };

    outer[outer.length - 1] = extractPoint(outer[outer.length - 1]);

    inner[0] = extractPoint(inner[0]);
    inner[0].cp1 = {
        x: (outer[outer.length - 1].x + inner[0].x) * 0.5,
        y: inner[0].y - 4 + (8 * this.armCoverage)
    };

    return {
        inner,
        outer
    }
}


export class SimpleArmletPart extends ClothingPart {
    constructor(...data) {
        super({
            layer     : Layer.ARMS,
            loc       : "+arm",
            aboveParts: ["parts arm", "decorativeParts arm", "parts hand"],
        }, {
            armCoverageTop: 0.8,
            armCoverage   : 0.9,
            thickness     : 1,

        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
        Clothes.simpleStrokeFill(ctx, ex, this);
        ctx.lineWidth = this.thickness;
        const {inner, outer} = calcArmSquare.call(this, ex);

        ctx.beginPath();
        drawPoints(ctx, ...outer, ...inner, outer[0]);
        ctx.fill();
        ctx.stroke();
    }
}


export class CrossedArmletPart extends ClothingPart {
    constructor(...data) {
        super({
            layer     : Layer.ARMS,
            loc       : "+arm",
            aboveParts: ["parts arm", "decorativeParts arm", "parts hand"],
        }, {
            armCoverageTop: 0.3,
            armCoverage   : 0.3,
            crossings     : 3,
            thickness     : 0.8,
            doubled       : true,
            fullArmlet    : false,
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
        Clothes.simpleStrokeFill(ctx, ex, this);
        ctx.lineWidth = this.thickness;
        const {inner, outer} = calcArmSquare.call(this, ex);

        if (this.fullArmlet) {
            ctx.beginPath();
            drawPoints(ctx, ...outer, ...inner, outer[0]);
            ctx.fill();
            ctx.stroke();
        }
        ;

        outer[outer.length - 1].cp1 = inner[0].cp1;
        //const lacing = getLacingPoints(topIn,topOut,botIn,botOut,this.crossings);
        const lacing = getLacingPoints(inner[inner.length - 1], outer[0], inner[0], outer[outer.length - 1],
            this.crossings);

        ctx.beginPath();
        if (this.doubled) {
            drawPoints(ctx, ...lacing.outer, breakPoint, ...lacing.inner);
        } else {
            drawPoints(ctx, ...lacing.outer);
        }
        ;
        ctx.stroke();
    }
}


export class SpiralArmletPart extends ClothingPart {
    constructor(...data) {
        super({
            layer     : Layer.ARMS,
            loc       : "+arm",
            aboveParts: ["parts arm", "decorativeParts arm", "parts hand"],
        }, {
            armCoverage: 0.3,
            coils      : 3,
            distance   : 5
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
        Clothes.simpleStrokeFill(ctx, ex, this);
        ctx.lineWidth = this.thickness;

        let adjustY = 0;
        const control = {
            x: 0,
            y: findBetween(ex.collarbone.y, ex.hand.palm.y, this.armCoverage)
        };

        for (var i = 1; i <= this.coils; i++) {
            coil(adjust(control, 0, adjustY), ctx);
            adjustY -= this.distance;
        }
        ;

        function coil(control, ctx) {
            const {inner, outer} = calcArm.call(this, ex, control);
            ctx.beginPath();
            drawPoints(ctx, inner, outer);
            ctx.stroke();
        };

    }
}


/**
 * Base Clothing classes
 */
export class Armlet extends Jewelry {
    constructor(...data) {
        super({

            clothingLayer: Clothes.Layer.BASE,
            stroke       : "#9c8530",
            fill         : "red",
        }, ...data);

    }
}

/**/


export class SpiralArmlet extends Armlet {
    constructor(...data) {
        super({}, ...data);
    }

    get partPrototypes() {
        return [
            {
				side: Part.RIGHT,
				Part: SpiralArmletPart,
            }
        ];
    }
}


export class SimpleArmlet extends Armlet {
    constructor(...data) {
        super({}, ...data);
    }

    get partPrototypes() {
        return [
            {
                side: Part.RIGHT,
                Part: SimpleArmletPart,
            }
        ];
    }
}

export class CrossedArmlet extends Armlet {
    constructor(...data) {
        super({}, ...data);
    }

    get partPrototypes() {
        return [
            {
                side: Part.RIGHT,
                Part: CrossedArmletPart,
            }
        ];
    }
}