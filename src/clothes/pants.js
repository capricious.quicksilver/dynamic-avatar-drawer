import {Clothes, ClothingPart, Clothing} from "./clothing";
import {Part} from "../parts/part";
import {Layer} from "../util/canvas";
import {
    simpleQuadratic,
    drawPoints,
    extractPoint,
    splitCurve,
    clamp,
} from "drawpoint/dist-esm";
import {adjustColor} from "../util/utility";

export class CoveredButtPart extends ClothingPart {
    constructor(...data) {
        super({
            layer     : Layer.BACK,
            reflect   : true,
            loc       : "butt",
            aboveParts: ["parts butt"],
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
        // scale to simulate clothing thickness
        Clothes.simpleStrokeFill(ctx, ex, this);
        ctx.strokeStyle =
            adjustColor(ctx.strokeStyle,
                {
                    s: -5,
                    l: -5
                });

        // top strap
        ctx.beginPath();
        drawPoints(ctx, ex.butt.in, ex.butt.out, ex.butt.in);
        ctx.fill();
        ctx.stroke();

    }
}


export class LongPantsPart extends ClothingPart {
    constructor(...data) {
        super({
            layer     : Layer.FRONT,
            loc       : "leg",
            aboveParts: ["parts leg"],
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
        let sp =
            splitCurve(this.legCoverage, ex.calf.out, ex.ankle.out);
        const outBot = sp.left.p2;

        sp = splitCurve(1 - this.legCoverage,
            ex.ankle.in, ex.calf.in);
        const outIn = sp.left.p2;
        const calfIn = sp.right.p2;

        outIn.cp1 = simpleQuadratic(outBot, outIn, 0.5, 1);

        const kneeOut = ex.knee.out;
        const kneeIn = ex.knee.in;
        const kneeInTop = ex.knee.intop;
        const calfOut = ex.calf.out;
        const thighTop = ex.thigh.top;
        const thighIn = ex.thigh.in;

        const {out, hip, top, thighOut, groin} =
            calcPantsTop(ex, this.waistCoverage);

        // scale to simulate clothing thickness
        Clothes.simpleStrokeFill(ctx, ex, this);

        // top strap
        ctx.beginPath();
        drawPoints(
            ctx, top, out, hip, thighOut, kneeOut, calfOut, outBot,
            outIn, calfIn, kneeIn, kneeInTop, thighIn, thighTop, groin);
        ctx.fill();
        ctx.stroke();
    }
}


export class MediumPantsPart extends ClothingPart {
    constructor(...data) {
        super({
            layer     : Layer.FRONT,
            loc       : "leg",
            aboveParts: ["parts leg"],
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
        let sp =
            splitCurve(this.legCoverage, ex.knee.out, ex.calf.out);
        const outBot = sp.left.p2;

        sp = splitCurve(1 - this.legCoverage,
            ex.calf.in, ex.knee.in);
        const outIn = sp.left.p2;
        const kneeIn = sp.right.p2;

        outIn.cp1 = simpleQuadratic(outBot, outIn, 0.75, 2);

        const kneeOut = ex.knee.out;
        const kneeInTop = ex.knee.intop;
        const thighTop = ex.thigh.top;
        const thighIn = ex.thigh.in;


        const {out, hip, top, thighOut, groin} =
            calcPantsTop(ex, this.waistCoverage);

        // scale to simulate clothing thickness
        Clothes.simpleStrokeFill(ctx, ex, this);

        // top strap
        ctx.beginPath();
        drawPoints(
            ctx, top, out, hip, thighOut, kneeOut, outBot, outIn,
            kneeIn, kneeInTop, thighIn, thighTop, groin);
        ctx.fill();
        ctx.stroke();

    }
}


export class ShortPantsPart extends ClothingPart {
    constructor(...data) {
        super({
            layer     : Layer.FRONT,
            loc       : "leg",
            aboveParts: ["parts leg"],
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
        let sp = splitCurve(0.35 + this.legCoverage * 0.6,
            ex.thigh.out, ex.knee.out);
        const outBot = sp.left.p2;

        sp = splitCurve(1 - this.legCoverage,
            ex.knee.intop, ex.thigh.in);
        const outIn = sp.left.p2;
        const thighIn = sp.right.p2;
        const thighTop = ex.thigh.top;

        outIn.cp1 = simpleQuadratic(
            outBot, outIn, 0.5, 1 / clamp(this.legCoverage, 0.3, 1));

        const {out, hip, top, thighOut, groin} =
            calcPantsTop(ex, this.waistCoverage);

        // scale to simulate clothing thickness
        Clothes.simpleStrokeFill(ctx, ex, this);

        // top strap
        ctx.beginPath();
        drawPoints(
            ctx, top, out, hip, thighOut, outBot, outIn, thighIn,
            thighTop, groin);
        ctx.fill();
        ctx.stroke();

    }
}


export class ShortsPart extends ClothingPart {
    constructor(...data) {
        super({
            layer              : Layer.FRONT,
            loc                : "leg",
            aboveParts         : ["parts leg", "parts torso", "decorativeParts torso"],
            belowSameLayerParts: ["torso"],
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
        let sp = splitCurve(0.4 + this.legCoverage * 0.55,
            ex.hip, ex.thigh.out);
        const outBot = sp.left.p2;

        sp = splitCurve(clamp(1.25 - this.legCoverage * 0.75, 0, 1),
            ex.thigh.in, ex.thigh.top
        );
        const outIn = sp.left.p2;
        const thighTop = sp.right.p2;

        outIn.cp1 = simpleQuadratic(
            outBot, outIn, 0.6, -3 / clamp(this.legCoverage, 0.3, 1));


        const {out, hip, top, groin} =
            calcPantsTop(ex, this.waistCoverage);

        // scale to simulate clothing thickness
        Clothes.simpleStrokeFill(ctx, ex, this);

        // top strap
        ctx.beginPath();
        drawPoints(
            ctx, top, out, hip, outBot, outIn, thighTop, groin);
        ctx.fill();
        ctx.stroke();
    }
}


function calcPantsTop(ex, waistCoverage) {
    let out, hip, thighOut;
    if (waistCoverage >= -0.1) {
        const sp = splitCurve(0.9 - waistCoverage, ex.waist, ex.hip);
        out = sp.left.p2;
        hip = sp.right.p2;
        thighOut = ex.thigh.out;
    } else {
        const sp =
            splitCurve(-0.1 - waistCoverage, ex.hip, ex.thigh.out);
        out = sp.left.p2;
        thighOut = sp.right.p2;
    }

    const waistCurve = (out.y - ex.hip.y);
    const top = {
        x: 0,
        y: ex.pelvis.y + waistCurve * 1.2
    };
    out.cp1 = {
        x: out.x * 0.5 + top.x * 0.5,
        y: top.y
    };

    const groin = extractPoint(ex.groin);
    groin.cp1 = {
        x: groin.x * 0.5 + ex.thigh.top.x * 0.5,
        y: groin.y
    };
    return {
        out,
        hip,
        top,
        thighOut,
        groin
    };
}

export class Pants extends Clothing {
    constructor(...data) {
        super({
            clothingLayer: Clothes.Layer.MID,
            /**
             * How far to extend the pant legs
             * 1 goes all the way down to ankles
             * 0.5 goes to knees
             * anything else is shorter
             */
            legCoverage  : 1,
            /**
             * Where along waist-hips does the top of the hips sit
             * Positive values means further towards the waist, 0 is at the
             * hips,
             * negative values means along hips to inner knee
             */
            waistCoverage: 0,
        }, ...data);
    }


    fill() {
        return "hsl(200, 65%, 10%)";
    }
}


export class LongTightPants extends Pants {
    constructor(...data) {
        super(...data);
    }

    get partPrototypes() {
        return [
            {
                side: null,
                Part: CoveredButtPart,
            },
            {
                side: Part.LEFT,
                Part: LongPantsPart
            },
            {
                side: Part.RIGHT,
                Part: LongPantsPart
            },
        ];
    }
}


export class MediumTightPants extends Pants {
    constructor(...data) {
        super(...data);
    }

    get partPrototypes() {
        return [
            {
                side: null,
                Part: CoveredButtPart,
            },
            {
                side: Part.LEFT,
                Part: MediumPantsPart
            },
            {
                side: Part.RIGHT,
                Part: MediumPantsPart
            },
        ];
    }
}


export class ShortTightPants extends Pants {
    constructor(...data) {
        super(...data);
    }

    get partPrototypes() {
        return [
            {
                side: null,
                Part: CoveredButtPart,
            },
            {
                side: Part.LEFT,
                Part: ShortPantsPart
            },
            {
                side: Part.RIGHT,
                Part: ShortPantsPart
            },
        ];
    }
}


export class TightShorts extends Pants {
    constructor(...data) {
        super(...data);
    }

    get partPrototypes() {
        return [
            {
                side: null,
                Part: CoveredButtPart,
            },
            {
                side: Part.LEFT,
                Part: ShortsPart
            },
            {
                side: Part.RIGHT,
                Part: ShortsPart
            },
        ];
    }
}

