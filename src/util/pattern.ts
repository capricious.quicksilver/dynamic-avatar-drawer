import {rad, diff} from "drawpoint/dist-esm";

/**
 * Created by Johnson on 2017-06-17.
 */
export const IMAGE_MAXSIZE = 100;

// canvas used for patterns
const patternCanvas = document.createElement("canvas");
patternCanvas.width = IMAGE_MAXSIZE;
patternCanvas.height = IMAGE_MAXSIZE;

const patternCtx = patternCanvas.getContext("2d");

// canvas used for scaling images
const imageCanvas = document.createElement("canvas");


/**
 * Cache of loaded patterns, lazily created as needed by getPattern.
 * Each one is either a function that returns either a color, gradient, pattern (all are statically cached),
 * or a function that returns either a color, gradient, or pattern (dynamically cached since the parameters depend
 * on draw exports).
 */
export const cachedPatterns = {};

/**
 * Patterns to load as soon as we want to draw. Populated by getPattern as necessary.
 * @type {Promise[]}
 */
export const patternLoadingQueue : Promise<void>[] = [];


/**
 * patternName -> pattern production function map
 * Populated by calling addPattern, and queried by getPattern if pattern hasn't been cached yet
 */
const producePattern = {};


/**
 * List names of available patterns to gettable by getPattern
 * @returns {string[]}
 */
export function listAvailablePatterns() {
    const patterns : string[] = [];
    for (let pat in producePattern) {
        if (producePattern.hasOwnProperty(pat)) {
            patterns.push(pat);
        }
    }
    return patterns;
}

export function getPatternFullName(patternName, patternSize) {
    return patternName + "." + patternSize;
}

export function getPatternBaseName(patternName) {
    const splitPoint = patternName.indexOf(".");
    if (splitPoint < 0) {
        return patternName;
    }
    return patternName.substr(0, splitPoint);
}

/**
 * Intermediate storage of a pattern object (cached). Can be loaded to get a render-able object.
 */
export class CachedPattern {
    public fullName: string;
    public patternName: string;
    public patternSize: number;

    constructor(data) {
        Object.assign(this, {
            patternName: "",
            patternSize: IMAGE_MAXSIZE,
        }, data);
        this.fullName = getPatternFullName(this.patternName, this.patternSize);

        if (this.patternName !== "") {
            if (producePattern.hasOwnProperty(this.patternName) === false) {
                throw new Error("Trying to get pattern that hasn't been added yet: " + this.patternName);
            }

            // loading for the first time so we create the promise
            if (!cachedPatterns.hasOwnProperty(this.fullName)) {
                console.log("create pattern " + this.fullName);
                patternLoadingQueue.push(
                    loadPattern(this.patternName, producePattern[this.patternName], this.patternSize));
                // return cachedPatterns[fullName];
            }
        }
    }

    getLoaded(ctx, ex) {
        const source = cachedPatterns[this.fullName];
        // need dynamic production
        if (typeof source === "function") {
            return source(ctx, ex);
        }
        return source;
    }
}

/**
 * Get a fill or stroke pattern. When used inside draw, guaranteed to supply a usable pattern.
 * Used outside of draw could result in unfulfilled promises.
 * @param {string} patternName Name of the pattern
 * @param {number} patternSize How large the image canvas should be for this pattern
 * @returns {CachedPattern} pattern method
 */
export function getPattern(patternName, patternSize = IMAGE_MAXSIZE) {
    // return wrapper around delayed call - we'll also serialize objects rather than functions
    return new CachedPattern({
        patternName,
        patternSize
    });
}

export function getLoadedPattern(pattern, ctx, ex) {
    if (isPattern(pattern)) {
        return pattern.getLoaded(ctx, ex);
    } else if (typeof pattern === "function") {
        // recursive call in case there are multiple wrapping function layers
        return getLoadedPattern.call(this, pattern.call(this, ctx, ex), ctx, ex);
    }
    return pattern;
}

export function addPattern(patternName, patternSource) {
    "use strict";
    if (producePattern[patternName]) {
        return;
    }
    producePattern[patternName] = patternSource;
}

/**
 * Load pattern from some source method (either function or an image path) asynchronously
 * @param {string} patternName
 * @param {(string|function)} patternSource
 * @param {number} patternSize How large the image canvas should be for this pattern
 * @returns {Promise} Promise to produce this pattern (and cache it)
 */
export function loadPattern(patternName, patternSource, patternSize) {
    // depending on what kind of pattern was put in, we have different promises to fulfill

    const fullName = getPatternFullName(patternName, patternSize);
    /**
     * Custom method of creating the pattern, such as anything requiring ctx
     */
    if (typeof patternSource === "function") {
        /**
         * We heuristically decide whether pattern can be cached or has to be dynamic
         * by looking at the number of parameters the function accepts.
         * If it's <= 1 parameters, we asssume it takes ctx only and can be cached
         * Else we assume it needs ex or some other parameters
         */
        if (patternSource.length <= 1) {
            return new Promise<void>((resolve) => {
                cachedPatterns[fullName] = patternSource(patternCtx);
                resolve();
            });
        } else {
            return new Promise<void>((resolve) => {
                cachedPatterns[fullName] = patternSource;
                resolve();
            });
        }

    } else {
        // load image for the first time and cache it
        return new Promise<void>((resolve, reject) => {
            const image = new Image();
            image.onload = () => {
                const scale = image.width / patternSize;
                const height = Math.round(image.height / scale);
                imageCanvas.width = patternSize;
                imageCanvas.height = height;

                const imageCtx = imageCanvas.getContext("2d")!;
                imageCtx.scale(1, -1);
                imageCtx.drawImage(image,
                    0,
                    0,
                    image.width,
                    image.height,
                    0,
                    0,
                    patternSize,
                    -height);
                // only cached after load; could be trying to load same pattern multiple times (should be OK)
                cachedPatterns[fullName] = imageCtx.createPattern(imageCanvas, "repeat");

                resolve();
            };
            image.onerror = () => {
                alert(Error("failed to load pattern '" + patternName + "' from " +
                            patternSource));
                reject();
            };
            image.crossOrigin = "anonymous";
            image.src = patternSource;
        });
    }
}

export function drawTattooPattern(tattoo, image, ctx, renderPosition) {
    // 180 degrees offset because our contexts are vertically flipped so positive y is up
    const toRotate = rad(tattoo.rotation);
    const scale = (tattoo.width) ? tattoo.width / image.width : 1;

    // needs to be larger such that it can include the image at any rotation
    const w = image.width;
    const h = image.height;
    let s = Math.max(w, h) * Math.sqrt(2);
    imageCanvas.width = s * scale;
    imageCanvas.height = s * scale;

    const imageCtx = imageCanvas.getContext("2d")!;
    imageCtx.scale(scale, scale);
    // now everything is in (w,h) coordinates
    // get to the middle so rotations are about the center of the image
    imageCtx.translate(s / 2, s / 2);
    imageCtx.rotate(toRotate);
    imageCtx.drawImage(image, -w / 2, -h / 2);

    // uncomment for debugging
    // document.body.appendChild(imageCanvas);

    const tempPos = ctx.transformPoint(imageCanvas.width / 2, imageCanvas.height / 2);
    const basePos = ctx.transformPoint(0, 0);
    const relativePos = diff(tempPos, basePos);

    // need to offset the width and height of the image itself
    ctx.drawImage(imageCanvas,
        renderPosition.x + relativePos.x,
        renderPosition.y + relativePos.y);
}

export function isPattern(colour) {
    return colour instanceof CachedPattern;
}
