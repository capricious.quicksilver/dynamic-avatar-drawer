import resolvePath from 'object-resolve-path';
import {adjust} from "drawpoint/dist-esm";

/**
 * Created by Johnson on 2017-06-17.
 */
/**
 * Extract the side component from a location string
 * @memberof module:da
 * @param {string} partLoc Location string of a part
 * @returns {(string|null)} Either the side string or null if no side indicated
 */
export function extractSideLocation(partLoc) {
    const loc = extractUnmodifiedLocation(partLoc);
    // should be first word separated by whitespace
    if (loc.indexOf(" ") < 0) {
        return null;
    }
    return loc.substr(0, loc.indexOf(" "));
}

/**
 * Extract the base component from a location string
 * @memberof module:da
 * @param {string} partLoc Location string of a part
 * @returns {string} The base component
 */
export function extractBaseLocation(partLoc) {
    const loc = extractUnmodifiedLocation(partLoc);
    // last word separated by whitespace
    if (loc.indexOf(" ") < 0) {
        return loc;
    }
    return loc.substr(loc.lastIndexOf(" ") + 1);
}

/**
 * Extract the underlying location (side + base) of a location after stripping it of
 * potential modifier characters, which are prepended (use + for non-restrictive, - for exclusive)
 * @memberof module:da
 * @param {string} partLoc location name of part
 */
export function extractUnmodifiedLocation(partLoc) {
    for (let c = 0; c < partLoc.length; ++c) {
        switch (partLoc[c]) {
        case "+":
        case "-":
            break;
        default:
            return partLoc.substr(c);
        }
    }
}

/**
 * Extract modifier characters (+ for non-restrictive, - for exclusive)
 * @memberof module:da
 * @param {string} partLoc Location name of part
 * @returns {string} Modifier characters for this location
 */
export function extractLocationModifier(partLoc) {
    let modifiers = "";
    for (let c = 0; c < partLoc.length; ++c) {
        switch (partLoc[c]) {
        case "+":
        case "-":
            modifiers += partLoc[c];
            break;
        default:
            break;
        }
    }
    return modifiers;
}

/**
 * List of all base locations (without side and modifiers)
 */
export const Location = {
    ARM      : "arm",
    CHEST    : "chest",
    TORSO    : "torso",
    BUTT     : "butt",
    FEET     : "feet",
    GROIN    : "groin",
    HAND     : "hand",
    HEAD     : "head",
    HAIR     : "hair",
    LEG      : "leg",
    NECK     : "neck",
    PENIS    : "penis",
    VAGINA   : "vagina",
    TESTICLES: "testicles",
    GENITALS : "genitals",
    EAR      : "ears",
    EYEBROW  : "brow",
    EYELASH  : "eyelash",
    EYELID   : "eyelid",
    EYES     : "eyes",
    IRIS     : "iris",
    LIPS     : "lips",
    MOUTH    : "mouth",
    NOSE     : "nose",
    PUPIL    : "pupil",
};

export function locationIsSideless(loc) {
    switch (loc) {
        // dropdown
    case Location.TORSO:
    case Location.HEAD:
    case Location.NECK:
    case Location.PENIS:
    case Location.TESTICLES:
    case Location.VAGINA:
    case Location.LIPS:
    case Location.MOUTH:
    case Location.NOSE:
        return true;
    default:
        return false;
    }
}

/**
 * Get a location relative to a drawpoint
 * @param ex
 * @param drawpoint
 * @param dx
 * @param dy
 */
export function locateRelativeToDrawpoint(ex, {drawpoint, dx, dy}) {
    const dp = resolvePath(ex, drawpoint);
    return adjust(dp, dx, dy);
}
