import {Layer} from "../util/canvas";
import {
    extractBaseLocation,
    getSideValue,
    locateRelativeToDrawpoint,
    patternLoadingQueue
} from "..";
import {Part} from "../parts/part";
import { DrawPoint } from "drawpoint/dist-esm";

export class Tattoo {
    public layer: number;
    public side: 1 | 0;
    public rotation: number;
    public width: number;
    public loc: string;
    public ignoreClip: boolean;

    public aboveParts: string[];
    public belowParts: string[];

    public name: string;
    public src: string;
    public relativeLocation: { drawpoint: DrawPoint, dx: number, dy: number };
    
    constructor(...data) {
        Object.assign(this, {
            layer     : Layer.BASE,
            side      : Part.RIGHT,
            rotation  : 0,
            width     : 20,
            loc       : "+arm",
            ignoreClip: false,
        }, ...data);

        // tattoos can never be exclusive
        if (!this.loc.startsWith("+")) {
            this.loc = "+" + this.loc;
        }

        const loc = extractBaseLocation(this.loc);

        this.aboveParts = [`parts ${loc}`, `decorativeParts ${loc}`];
        this.belowParts = [`clothingParts ${loc}`];

        // canonize side value
        this.side = getSideValue(this.side);

        if (this.hasOwnProperty("name") === false) {
            throw new Error("Constructing tattoo with no name!");
        }
        if (this.hasOwnProperty("src") === false) {
            throw new Error("Constructing tattoo with no src!");
        }
        if (this.hasOwnProperty("relativeLocation") === false) {
            throw new Error("Constructing tattoo without relativeLocation object");
        }
    }

    /**
     * Return the starting location for drawing the item
     * @param ex Drawing exports
     */
    renderTattooLocation(ex) {
        return locateRelativeToDrawpoint(ex, this.relativeLocation);
    }
}

const cachedTattoos = {};

/**
 * Very similar to Items
 */
export const Tattoos = {
    getRender(tattoo) {
        if (cachedTattoos.hasOwnProperty(tattoo.name)) {
            return cachedTattoos[tattoo.name];
        }
        throw new Error("Tattoo not loaded yet: " + tattoo.name);
    },

    loadTattoo(tattoo) {
        if (cachedTattoos.hasOwnProperty(tattoo.name)) {
            return;
        }

        // create the promise to be loaded before we draw the tattoo
        patternLoadingQueue.push(new Promise<void>((resolve, reject) => {
            const image = new Image();
            image.onload = () => {
                cachedTattoos[tattoo.name] = image;
                resolve();
            };
            image.onerror = () => {
                console.log(Error("failed to load " + tattoo.src));
                reject();
            };
            image.crossOrigin = "anonymous";
            image.src = tattoo.src;
        }));
    },

    create(Item, ...data) {
        return new Tattoo(...data);
    }

};