---
layout: post
title:  "Introduction and version 0.13.2"
date:   2016-03-12 18:56:24 -0500
categories: update
---
The purpose of this site is to provide a centralized and up to date source for Dynamic Avatar Drawer.
Download and Test tabs will always have the latest versions of the drawer, and with each
update I'll write a post describing the changes.

For those new to this, DAD (Dynamic Avatar Drawer) is a tool primarily for web game developers
that prefer to work with text rather than raster graphics. It lets you visualize a broad range
of characters just by changing their statistics. The tool is meant to be embedded inside a Javascript
driven game that interacts with users via a browser (since it draws to an HTML canvas).

It comes as a Javascript module (the `da` module), and the [usage guide]({{site.baseurl}}/usage.html) 
details how to use and extend it. The [Twine guide]({{site.baseurl}}/twine.html) details how to specifically 
embed it inside a Twine game (to be added). Play around with it live through the [Test tab]({{site.baseurl}}/test.html)

A demo Twine game is in the works, as well as more facial variation in eyes and noses.