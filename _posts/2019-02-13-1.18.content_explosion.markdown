---
layout: post
title:  "1.18 Content Explosion (Dahakma)"
date:   2019-02-13
categories: release
---

- new clothing from Dahakma: 
    - dresses
    - corsets
    - tops
    - socks
    - chokers, collars
    - pants
- fixed transparent clothing double shading

Huge thanks to Dahakma for their work on creating so many assets!
All of these are templates with their own unique parameters, play around with them in the playground.

The usage guide will be expanded with a creator's guide to help people create content.

![preview](https://i.imgur.com/IWUnrdg.png)